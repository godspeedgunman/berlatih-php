<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
<<<<<<< HEAD
use App\Pertanyaan;
=======
>>>>>>> 82362d477310ac60c854adeb8238d8951398e92d

class PertanyaanController extends Controller
{
    public function create(){
        return view ('pertanyaan.create');
    }

    public function store(Request $request){
        $request->validate([
            'title' => 'required|unique:pertanyaan',
            'body' => 'required'
        ]);

        // $query = DB::table('pertanyaan')->insert([
        //     "title" => $request["title"],
        //     "body" => $request["body"]
        // ]);

        $pertanyaan = new Pertanyaan;
        $pertanyaan->title = $request["title"];
        $pertanyaan->body - $request["body"];
        $pertanyaan->save();

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan!');
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('posts'));    
    }

    public function show($id){
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required|unique:pertanyaan',
            'body' => 'required',
        ]);

        $query = DB::table('pertanyaan')
            ->where('id', $id)
            ->update([
                'title' => $request["title"],
                'body' => $request["body"]
            ]);
        return redirect('/pertanyaan');
    }

    public function destroy($id)
    {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan');
    }

}
