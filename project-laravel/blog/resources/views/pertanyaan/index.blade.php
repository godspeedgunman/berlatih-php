@extends('adminlte.master')

@section('content')
    <a href="/pertanyaan/create" class="btn btn-primary">Tambah</a>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Body</th>
            <th scope="col" style="display: inline">Actions</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($post as $key=>$pertanyaan)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$pertanyaan->title}}</td>
                    <td>{{$pertanyaan->body}}</td>
                    <td>
                        <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info">Show</a>
                        <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" pertanyaan="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
@endsection